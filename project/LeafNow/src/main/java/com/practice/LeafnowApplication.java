package com.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeafnowApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeafnowApplication.class, args);
	}

}

package com.practice.contactus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.contactus.bean.ContactUsBean;
import com.practice.contactus.service.ContactUsService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/CONTACT-US")
public class ContactUsController {

	@Autowired
	ContactUsService contactUsService;

	@PostMapping(value = "/CreateContactUs")
	public void createContactUs(@RequestBody ContactUsBean contactUsBean) {
		contactUsService.createContactUs(contactUsBean);
	}

	@DeleteMapping(value = "/DeleteContactUsById/{id}")
	public void deleteContactUsbyId(@PathVariable("id") int id) {
		contactUsService.deleteContactUsbyId(id);
	}

	@PostMapping(value = "GetContactUsByID/{id}")
	public ContactUsBean getContactUsbyId(@PathVariable("id") int id) {
		return contactUsService.getContactUsbyId(id);
	}

	@GetMapping(value = " GetListOfcontactUs")
	public List<ContactUsBean> getListOfContactUsList() {
		return contactUsService.getListOfContactUsList();
	}

}

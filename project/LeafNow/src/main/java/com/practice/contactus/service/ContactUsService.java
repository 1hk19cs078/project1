package com.practice.contactus.service;

import java.util.List;

import com.practice.contactus.bean.ContactUsBean;

public interface ContactUsService {
	public void createContactUs(ContactUsBean contactUsBean);

	public void deleteContactUsbyId(int id);

	public ContactUsBean getContactUsbyId(int id);

	public List<ContactUsBean> getListOfContactUsList();
}

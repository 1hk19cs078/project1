package com.practice.contactus.repository;

import org.springframework.data.repository.CrudRepository;

import com.practice.contactus.entity.ContactUsEntity;

public interface ContactUsRepository extends CrudRepository<ContactUsEntity, Integer> {

}

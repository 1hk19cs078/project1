package com.practice.contactus.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.contactus.bean.ContactUsBean;
import com.practice.contactus.entity.ContactUsEntity;
import com.practice.contactus.repository.ContactUsRepository;
import com.practice.contactus.service.ContactUsService;

@Service
public class ContactUsServiceImpl implements ContactUsService {

	@Autowired
	ContactUsRepository contactUsRepository;

	@Override
	public void createContactUs(ContactUsBean contactUsBean) {
		ContactUsEntity contactUsEntity = null;
		if (contactUsBean != null) {
			contactUsEntity = new ContactUsEntity();
			BeanUtils.copyProperties(contactUsBean, contactUsEntity);
			contactUsRepository.save(contactUsEntity);
		}
	}

	@Override
	public List<ContactUsBean> getListOfContactUsList() {
		List<ContactUsBean> listOfContactUsBean = null;
		List<ContactUsEntity> listOfContactUsEntity = (List<ContactUsEntity>) contactUsRepository.findAll();
		listOfContactUsBean = new ArrayList<>();
		if (listOfContactUsEntity != null && !"".equals(listOfContactUsEntity)) {
			for (ContactUsEntity contactUsEntity : listOfContactUsEntity) {
				ContactUsBean contactUsBean = new ContactUsBean();
				BeanUtils.copyProperties(contactUsEntity, contactUsBean);
				listOfContactUsBean.add(contactUsBean);
			}

		}
		return listOfContactUsBean;

	}

	@Override
	public void deleteContactUsbyId(int id) {
		if (id != 0) {
			contactUsRepository.deleteById(id);
		}

	}

	@Override
	public ContactUsBean getContactUsbyId(int id) {
		ContactUsBean contactUsBean = new ContactUsBean();
		if (id != 0) {
			Optional<ContactUsEntity> contactUsEntity = contactUsRepository.findById(id);

			BeanUtils.copyProperties(contactUsEntity.get(), contactUsBean);

		}
		return contactUsBean;
	}

}

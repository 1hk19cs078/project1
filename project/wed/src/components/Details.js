import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
export default class Details extends Component{
    submitHandler= () => {
          this.props.history.push({
            pathname: "/sign-in",
    
          });
        
      }
    render(){
        
        return(
            <React.Fragment>
                <div className="detail-page-inner"   >
            <Form onSubmit={this.submitHandler} >
            <h3>Student Details</h3>

            <div className="form-group">
                <label>USN :</label>
                <input type="text" className="form-control" placeholder="Enter USN" />
            </div>

            <div className="form-group">
                <label>Name :</label>
                <input type="password" className="form-control" placeholder="Enter Name" />
            </div>
            <div className="form-group">
                <label>Date Of Birth :</label>
                <input type="password" className="form-control" placeholder="Enter Dob" />
            </div>
            <div className="form-group">
                <label>Semister 1 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 2 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 3 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 4 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 5 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 6 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 7 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
            <div className="form-group">
                <label>Semister 8 Marksheet:</label>
                <input type="password" className="form-control" />
                <button className="btn btn-primary btn-block" style={{width:"13%", float:"right"}}>Upload</button>
            </div>
           
            <button onClick={ this.submitHandler} type="button" className="btn btn-primary btn-block" style={{width:"34%",marginLeft:"30%"}}>Submit</button>
            <hr/>
        </Form>
        </div>
        </React.Fragment>
        );
    }
}
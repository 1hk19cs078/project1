import React, { Component } from "react";
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { GrView } from 'react-icons/gr';
export default class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: '',
            email:'',
            lastName:'',
            password:'',
            usn:'',
            hidden: true,
             loading: false, error: "", flag: 0,
        }
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.toggleShow = this.toggleShow.bind(this);
      }
      changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value })
      }
      handlePasswordChange(e) {
        this.setState({ password: e.target.value });
      }
      toggleShow() {
        this.setState({ hidden: !this.state.hidden });
      }
      componentDidMount() {
        if (this.props.password) {
          this.setState({ password: this.props.password });
        }
      }
      UserHandler(user) {
        if (user === "Details") {
          this.props.history.push({
            pathname: "/Details",
    
          });
        }
      }
  submitHandler = () => {
    console.log("submit handler");
    console.log(this.state)
    axios.post('http://127.0.0.1:8081/CONTACT-US/CreateContactUs', this.state)
      .then(response => {
        console.log('response maaz', response.status)
        if (response.status === 200) {
            console.log('maaz');
            this.props.history.push({
                pathname: "/Details",
    
              });
          }
      })


      .catch(error => {
        console.log(error)
        this.setState({
          flag: 0,
        })
      });
   
  }
    render() {
        return (
            <React.Fragment>
 <div className="auth-inner">
            <Form onSubmit={this.submitHandler} >
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>First name</label>
                    <input type="text" name="firstName" onChange={this.changeHandler} className="form-control" placeholder="First name" />
                </div>

                <div className="form-group">
                    <label>Last name</label>
                    <input type="text" name="lastName" onChange={this.changeHandler} className="form-control" placeholder="Last name" />
                </div>

                <div className="form-group">
                    <label>USN</label>
                    <input type="text" name="usn" onChange={this.changeHandler} className="form-control" placeholder="USN" />
                </div>
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" name="email" onChange={this.changeHandler} className="form-control" placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input
                     type={this.state.hidden ? 'password' : 'text'}
                     value={this.state.password}
                     onChange={this.handlePasswordChange}
                    //  ref={inputRef}
                     name="password"  
                    //  onChange={this.changeHandler} 
                     className="form-control"
                      placeholder="Enter password" >
                     
                      </input>
                      <GrView  onClick={this.toggleShow}/>
                </div>

                <button  onClick={ this.submitHandler} type="button" className="btn btn-primary btn-block">Sign Up</button>

                <p className="forgot-password text-right">
                    Already registered <a href="/sign-in">sign in?</a>
                </p>
            </Form>
            </div>
            </React.Fragment>
        );
    }
}
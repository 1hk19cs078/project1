import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

export default class Login extends Component {
    UserHandler(user) {
        if (user === "sign-up") {
          this.props.history.push({
            pathname: "./sign-up",
    
          });
        }
      }
      submitHandler= () => {
        this.props.history.push({
          pathname: "/Details",
  
        });
      
    }
      
    render() {
        return (
            <div className="auth-inner">
            <form onSubmit={this.submitHandler} >
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>USN</label>
                    <input type="text" className="form-control" placeholder="Enter USN" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                    
                </div>

                <button  onClick={ this.submitHandler} type="button" className="btn btn-primary btn-block">Login</button>
                {/* console.log("maaz") */}
                <p className="forgot-password text-right">
                    Forgot <a href="#">password?</a>
                </p>
                <hr/>
                <Link className="nav-link" to={"/sign-up"}>Sign up</Link>

                
            </form>
            </div>
        );
    }
}
